---
layout: page
title: Contribute
sorted: 5
---

# Table of Contents
{:.no_toc}

* Do not remove this line (it will not be displayed)
{:toc}

***

# Merge Requests

Kirogi's source code is [hosted on KDE's GitLab](https://invent.kde.org/kde/kirogi). You can fork and make merge requests via the website.

We try our best to review submissions quickly, but have a little patience with our small team. &#x1f642;

# Source Tree Layout

Below is a list of directory paths in Kirogi's source tree and their purpose, to help you navigate our codebase.

- `cmake/` - Additional CMake modules for dependency discovery.
- `data/` - Metadata including the .desktop file, AppStream data and the application icon.
- `platforms/android/` - Boilerplate and assets specific to the Android version, including the splash screen.
- `src/` - The primary source code of the application, including the classic `main.cpp`.
- `src/gstreamer/` - Helper code to integrate the GStreamer multimedia framework into the application.
- `src/lib/` - The backend library used by the application, which can be built seperately and used by other applications as well. It has C++ and Qt Quick APIs and a plugin interface.
- `src/plugins/` - Plugins implementing support for various vehicles.
- `src/ui/` - The QML files implementing the application user interface.
- `src/ui/assets/` - Static image assets used in the UI.
- `src/ui/components/` - QML components used repeatedly throughout the UI.
- `src/ui/helpers/` - Little helpers used internally by the QML components.

# Build Instructions

## General

Kirogi is a KDE application using KDE's standard CMake-based build system. KDE's [build documentation](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source) applies to Kirogi, but here are some example build commands you can adapt to suit your needs:

```
cd kirogi
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=relwithdebinfo -DCMAKE_INSTALL_PREFIX=<path to install to> ../
make
make install
```

Kirogi's build system supports building the backend library only if you're building a new application using its backend library. Check the top-level [`CMakeLists.txt`](https://invent.kde.org/kde/kirogi/blob/master/CMakeLists.txt) file for build options.

If you are including the [MAVLink](https://en.wikipedia.org/wiki/MAVLink) plugin for the build, then the CMake command would be:

```
cmake -DCMAKE_BUILD_TYPE=relwithdebinfo -DCMAKE_INSTALL_PREFIX=<path to install to> -DMAVLINK_INCLUDE_DIR<path MAVLink is installed to> ../
```

## Android

For local development builds targeting Android, we recommend the [KDE Android SDK Docker image](https://community.kde.org/Android/Environment_via_Container). Its pre-installed software and helper scripts make building Kirogi and its dependencies fast and easy.

# Dependencies

Kirogi is written in C++14 and QML.

## General

Kirogi requires recent versions of Qt and some of the [KDE Frameworks](https://kde.org/products/frameworks/) libraries. For a complete list of dependencies, check the top-level [`CMakeLists.txt`](https://invent.kde.org/kde/kirogi/blob/master/CMakeLists.txt) file in the codebase, or just try to build the application and follow the instructions in the build output in case of missing dependencies.

Kirogi also requires a recent version of the [GStreamer](https://gstreamer.freedesktop.org/) multimedia framework and in particular the `gstqmlgl` sink plugin in `gst-plugins-good`.

## Android

On Android, Kirogi currently additionally depends on [QtZeroConf](https://github.com/jbagg/QtZeroConf) for mDNS-based vehicle discovery (desktop builds use [KDNSSD](https://api.kde.org/frameworks/kdnssd/html/index.html) instead).

# License

Kirogi's library in <a href="https://invent.kde.org/kde/kirogi/tree/master/src/lib">src/lib/</a> and bundled vehicle support plugins in <a href="https://invent.kde.org/kde/kirogi/tree/master/src/plugins/">src/plugins/</a> are licensed under LGPL v2.1 (or later), see COPYING.LIB.

The frontend (UI and other business logic) is licensed under GPL v2 (or later), see <a href="https://invent.kde.org/kde/kirogi/blob/master/COPYING">COPYING</a>.

The app icon in <a href="https://invent.kde.org/kde/kirogi/tree/master/data/icons">data/icons/</a> is licensed under LGPL v2.1 (or later), see <a href="https://invent.kde.org/kde/kirogi/blob/master/COPYING.LIB">COPYING.LIB</a>.

The mascot artwork in <a href="https://invent.kde.org/kde/kirogi/tree/master/src/ui/assets/">src/ui/assets/</a> is licensed under CC-BY-SA 4.0, see <a href="https://invent.kde.org/kde/kirogi/blob/master/COPYING.MASCOT">COPYING.MASCOT</a>.
